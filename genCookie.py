import urllib
import sys
import urllib2
import json
import time
from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_v1_5
import base64
import cookielib
import getpass

uname = raw_input("user:")
passwd = getpass.getpass()
user_agent = 'Mozilla/4.0 (compatible; MSIE 5.5; Windows NT)'

baseurl = "https://partner.steampowered.com"

# Request key
url = baseurl + '/login/getrsakey/'
values = {'username' : uname, 'donotcache' : str(int(time.time()*1000))}
headers = { 'User-Agent' : user_agent }
post = urllib.urlencode(values)
req = urllib2.Request(url, post, headers)
response = urllib2.urlopen(req).read()
data = json.loads(response)

print "Get Key Success:", data["success"]

# Encode key
mod = long(str(data["publickey_mod"]), 16)
exp = long(str(data["publickey_exp"]), 16)
rsa = RSA.construct((mod, exp))
cipher = PKCS1_v1_5.new(rsa)

#print base64.b64encode(cipher.encrypt(passwd))

print "code:"

sys.stdout.flush()

twofactor = raw_input()

# Login
url2 = baseurl + '/login/dologin/'
values2 = {
        'username' : uname,
        "password": base64.b64encode(cipher.encrypt(passwd)),
        "twofactorcode": twofactor,
        "emailauth": "",
        "loginfriendlyname": "",
        "captchagid": "-1",
        "captcha_text": "",
        "emailsteamid": "",
        "rsatimestamp": data["timestamp"],
        "remember_login": False,
        "donotcache": str(int(time.time()*1000)),
}
headers2 = { 'User-Agent' : user_agent }
post2 = urllib.urlencode(values2)
req2 = urllib2.Request(url2, post2, headers)
openurl = urllib2.urlopen(req2)
response2 = openurl.read()
data2 = json.loads(response2)

if data2["success"]:
        print "Logged in!"
else:
        print "Error, could not login:", data2["message"]

info = openurl.info()

goodCookie = openurl.info()['Set-Cookie'].split(' ')[3]
f = open("cookie", 'w')
f.write(goodCookie)
f.close()
