import urllib
import re
import code
import sys
import urllib2
import json
import time
import base64
import cookielib
from BeautifulSoup import BeautifulSoup

class InfoGetter:
    def getInfo(self):

        baseurl = "https://partner.steampowered.com"

        f = open("cookie", 'r')
        goodCookie = f.read()
        f.close()

        req = urllib2.Request(baseurl + '/app/details/655950/?dateStart=2000-01-01&dateEnd=today')
        req.add_header("Cookie", goodCookie)

        sock = urllib2.urlopen(req)

        html = sock.read()

        sock.close()

        parsed = BeautifulSoup(html)

        regex = re.compile(r"^\s*\$*\d+%*\+*\s*$")

        ogGoals = ["Steam units", "Retail activations", "Steam revenue", "Wishlists"]

        regexd = {}
        for goal in ogGoals:
            regexd[goal] = re.compile("^" + goal + "$")

        successes = []
        vals = []

        #goals = ogGoals[:]

        lines = parsed.body.findAll('td')
        #code.interact(local=locals())

        i = 0
        while i < len(lines) - 8:
            #j = 0
            #print lines[i]
            #print lines[i].text
            #print "still finding "
            #print regexd
            keysToDel = []
            for key in regexd:
                if regexd[key].search(lines[i].text) != None:
                    #print goals[j]
                    successes.append(key)
                    keysToDel.append(key)
                #j += 1
            for key in keysToDel:
                del regexd[key]
            if len(vals) != len(successes):
                #print "looky"
                if regex.search(lines[i].text.replace(",", "")) != None:
                        #print lines[i].text
                        vals.append(lines[i].text)

            i += 1



        #print successes
        #print vals
        return dict(zip(successes, vals))
        #print sales


if __name__=="__main__":
    print InfoGetter().getInfo()
