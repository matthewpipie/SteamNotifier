import useCookie
import json
import os.path
import smtplib
 
def sendemail(from_addr, to_addr_list, cc_addr_list,
              subject, body,
              login, password,
              smtpserver='smtp.gmail.com:587'):
    header  = 'From: %s\n' % from_addr
    header += 'To: %s\n' % ','.join(to_addr_list)
    header += 'Cc: %s\n' % ','.join(cc_addr_list)
    header += 'Subject: %s\n\n' % subject
    message = header + body
 
    server = smtplib.SMTP(smtpserver)
    server.starttls()
    server.login(login,password)
    problems = server.sendmail(from_addr, to_addr_list, message)
    server.quit()


info = useCookie.InfoGetter().getInfo()

SERVER = "gmail.com"
USER = "ExocometsDev"
f = open('emailpass', 'r')
PASS = f.read()
f.close()
TO = ["matthewpipie@gmail.com", "tpgiordano9@gmail.com", "Epesikoff@gmail.com"]

if os.path.exists('info'):
    f = open("info", 'r')
    oldInfo = json.load(f)
    f.close()
else:
    oldInfo = {}

if oldInfo != info and len(info) != 0:
    print "They are not equal! writing "
    print info
    f = open("info", "w")
    json.dump(info, f)
    f.close()
    sendemail(
        from_addr    = USER + "@" + SERVER,
        to_addr_list = TO,
        cc_addr_list = [],
        subject      = 'Exocomets Update', 
        body         = 'New info: ' + json.dumps(info, indent=4, sort_keys=True, separators=('', ': ')),
        login        = USER, 
        password     = PASS
    )

else:
    #pass
    print "equal"
